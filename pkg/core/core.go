package core

import (
	"context"
	"log/slog"
	"strings"
	"sync"

	"anime.bike/irc/pkg/ircev"
	"anime.bike/irc/pkg/ircv3"
	dll "github.com/emirpasic/gods/v2/lists/doublylinkedlist"
)

/*
daemon is the backend of the gui application
guis are actually for communicating with a daemon
*/
type Daemon struct {
	backends dll.List[*Backend]
}

/*
backend represents a connection from the daemon to a single remote.
*/
type Backend struct {
	config BackendConfig
	log    *slog.Logger

	// might be needed? not sure yet.
	// the currently negotiated caps
	m ircev.MessageHandler

	mu          sync.RWMutex
	caps        map[string]*ircv3.Capability
	enabledCaps []*Cap
}

func NewBackend(config BackendConfig, log *slog.Logger) *Backend {
	b := &Backend{
		config: config,
		caps:   make(map[string]*ircv3.Capability),
		log:    log,
	}
	b.createMux()
	return b
}

func (b *Backend) createMux() {
	m := &ircev.Mux{}
	b.m = m
	m.HandleFunc()(
		func(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error {
			b.log.Debug("recv msg", "msg", msg)
			return nil
		})

	//things below here dont need to happen if caps are disabled :)
	if b.config.DisableCap302 {
		return
	}
	onCapLs := func(ctx context.Context, codec ircv3.SendCodec) error {
		if b.config.Authn != nil {
			err := b.config.Authn.OnMessage(ctx, codec, nil)
			if err != nil {
				return err
			}
		}
		for _, v := range b.config.Capabilities {
			if v.Matcher(b.caps) {
				err := v.Handler.OnMessage(ctx, codec, nil)
				if err != nil {
					return nil
				}
			}
		}
		// TODO: trigger / enable the caps based off name
		return codec.Send(ctx, ircv3.NewMessage("CAP END"))
	}

	m.CmdFunc("CAP", ircev.ParamEqual(0, "*"), ircev.ParamEqual(1, "LS"))(
		func(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error {
			capStrings := strings.Split(msg.Param(2), " ")
			for _, v := range capStrings {
				capStruct := ircv3.ParseCapability(v)
				b.caps[capStruct.Name] = capStruct
			}
			return onCapLs(ctx, codec)
		},
	)

	if b.config.Authn != nil {
		m.Cmd("AUTHENTICATE")(b.config.Authn)
	}

	// TODO: enable the cap handlers
	m.HandleFunc()(func(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error {
		b.mu.RLock()
		defer b.mu.RUnlock()
		for _, v := range b.enabledCaps {
			err := v.Handler.OnMessage(ctx, codec, msg)
			if err != nil {
				return err
			}
		}
		return nil
	})
}

func (b *Backend) OnConnect(ctx context.Context, codec ircv3.SendCodec) error {
	if !b.config.DisableCap302 {
		codec.Send(ctx, ircv3.NewMessage("CAP", "LS", "302"))
	}
	u := b.config.User
	if err := codec.Send(ctx, ircv3.NewMessage("NICK", u.Nick)); err != nil {
		return err
	}
	codec.Send(ctx, ircv3.NewMessage("USER", u.Username, u.Hostname, u.Server, u.Realname))
	return nil
}

func (b *Backend) OnMessage(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error {
	return b.m.OnMessage(ctx, codec, msg)
}

func (b *Backend) OnDisconnect(err error) error {
	return nil
}

type User struct {
	Nick string

	Username string
	Realname string
	Hostname string
	Server   string
}

type BackendConfig struct {
	DisableCap302 bool
	User          User
	Authn         ircev.MessageHandler
	Capabilities  []*Cap
}
