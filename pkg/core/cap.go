package core

import (
	"anime.bike/irc/pkg/ircev"
	"anime.bike/irc/pkg/ircv3"
)

type Cap struct {
	// requires one of matcher to enable handler
	Matcher func(map[string]*ircv3.Capability) bool
	// handler, which will be invoked with nil message
	// on CAP LS or on async cap negotiation if matcher matches
	Handler ircev.MessageHandler
}
