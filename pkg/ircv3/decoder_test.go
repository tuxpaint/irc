package ircv3_test

import (
	"strings"
	"testing"

	"anime.bike/irc/pkg/ircv3"
	"github.com/stretchr/testify/require"
)

func TestDecoder(t *testing.T) {
	dec := ircv3.Decoder{}

	buf := strings.NewReader("@some :source HELLO")
	msg := new(ircv3.Message)
	err := dec.Decode(buf, msg)

	require.NoError(t, err)

	require.EqualValues(t, "HELLO", msg.Command)
	require.EqualValues(t, "", msg.Tags["some"])
}
