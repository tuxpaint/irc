package ircv3

import "strings"

type Capability struct {
	Name   string
	Values []string
}

func (c *Capability) String() string {
	o := new(strings.Builder)
	o.WriteString(c.Name)
	if len(c.Values) > 0 {
		o.WriteRune('=')
		// TODO: could be more efficient but whatever
		o.WriteString(strings.Join(c.Values, ","))
	}
	return o.String()
}

func ParseCapability(s string) *Capability {
	c := &Capability{}
	split := strings.SplitN(s, "=", 2)
	if len(split) == 1 {
		c.Name = s
	} else {
		c.Name = split[0]
		c.Values = strings.Split(split[1], ",")
	}
	return c
}
