package ircv3

import (
	"io"

	"github.com/valyala/bytebufferpool"
)

type Encoder struct {
}

// write a message to the stream
func (e *Encoder) Encode(w io.Writer, msg *Message) error {
	b := bytebufferpool.Get()
	defer bytebufferpool.Put(b)
	if err := msg.Encode(b); err != nil {
		return err
	}
	b.WriteString("\r\n")
	_, err := b.WriteTo(w)
	if err != nil {
		return err
	}
	return nil
}
