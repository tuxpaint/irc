package ircv3

import (
	"io"
	"strings"
)

// tags are stored as strings
type Tags map[string]string

func (h Tags) Set(key, value string) {
	h[key] = value
}

func (h Tags) Get(key string) string {
	val, ok := h[key]
	if !ok {
		return ""
	}
	return val
}

func (h Tags) Keys() []string {
	o := make([]string, 0, len(h))
	for k := range h {
		o = append(o, k)
	}
	return o
}
func (h Tags) ClientOnlyKeys() []string {
	o := make([]string, 0)
	for k := range h {
		if IsClientOnly(k) {
			o = append(o, k)
		}
	}
	return o
}

func IsClientOnly(xs string) bool {
	return strings.HasPrefix(xs, "+")
}

func (h Tags) WriteTo(w io.Writer) (int64, error) {
	var nn int64
	n, err := w.Write([]byte{'@'})
	nn += int64(n)
	if err != nil {
		return nn, err
	}
	firstTag := true
	for k, v := range h {
		if !firstTag {
			n, err = w.Write([]byte(";"))
			nn += int64(n)
			if err != nil {
				return nn, err
			}
		}
		firstTag = false
		n, err := w.Write([]byte(k))
		nn += int64(n)
		if err != nil {
			return nn, err
		}
		if len(v) > 0 {
			n, err = w.Write([]byte("=" + EscapeTagValue(v)))
			nn += int64(n)
			if err != nil {
				return nn, err
			}
		}
	}
	return nn, nil

}
