package ircv3

import (
	"bufio"
	"context"
	"fmt"
	"net"
	"sync"
)

type SendCodec interface {
	Send(context.Context, *Message) error
}
type ReceiveCodec interface {
	Receive(context.Context, *Message) error
}

type Codec interface {
	SendCodec
	ReceiveCodec
}

type connCodec struct {
	conn net.Conn
	r    *bufio.Reader
	mu   sync.Mutex

	e Encoder
	d Decoder
}

func NewConnCodec(conn net.Conn) Codec {
	return &connCodec{
		conn: conn,
		r:    bufio.NewReader(conn),
	}
}

func (c *connCodec) Send(ctx context.Context, msg *Message) error {
	errCh := make(chan error)
	go func() {
		defer close(errCh)
		// TODO: maybe set the conn timeout?
		err := c.e.Encode(c.conn, msg)
		if err != nil {
			errCh <- err
			return
		}
	}()
	select {
	case <-ctx.Done():
		// TODO: should we close on timeout?
		c.conn.Close()
		return ctx.Err()
	case err := <-errCh:
		if err != nil {
			return err
		}
		return nil
	}
}
func (c *connCodec) Receive(ctx context.Context, msg *Message) error {
	if msg == nil {
		return fmt.Errorf("passed nil msg to receive")
	}
	errCh := make(chan error)
	go func() {
		defer close(errCh)
		// TODO: maybe set the conn timeout?
		err := c.d.Decode(c.conn, msg)
		if err != nil {
			errCh <- err
			return
		}
	}()
	select {
	case <-ctx.Done():
		// TODO: should we close on timeout?
		c.conn.Close()
		return ctx.Err()
	case err := <-errCh:
		if err != nil {
			return err
		}
		return nil
	}
}
