package ircv3

import "errors"

var ErrDecodeMessage = errors.New("decoding message")
