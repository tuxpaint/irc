package ircev

import (
	"context"

	"anime.bike/irc/pkg/ircv3"
)

type ClientStage = int32

type ClientHandler interface {
	OnConnect(context.Context, ircv3.SendCodec) error
	OnDisconnect(err error) error
	MessageHandler
}

type Client struct {
	handlers ClientHandler
}

func NewClient(handlers ClientHandler) *Client {
	c := &Client{
		handlers: handlers,
	}
	return c
}

func (c *Client) Start(ctx context.Context, codec ircv3.Codec) (err error) {
	err = c.handlers.OnConnect(ctx, codec)
	if err != nil {
		return err
	}
	defer func() {
		err = c.handlers.OnDisconnect(err)
	}()
	for {
		msg := &ircv3.Message{}
		err := codec.Receive(ctx, msg)
		if err != nil {
			return err
		}
		err = c.handlers.OnMessage(ctx, codec, msg)
		if err != nil {
			return err
		}
	}
}
