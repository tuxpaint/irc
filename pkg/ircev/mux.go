package ircev

import (
	"context"

	"anime.bike/irc/pkg/ircv3"
)

type MessageHandler interface {
	OnMessage(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error
}
type MessageHandlerFunc func(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error

func (m MessageHandlerFunc) OnMessage(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error {
	return m(ctx, codec, msg)
}

// a route is matched based off command, then an arbitrary amount of parameter matchers
type Mux struct {
	routes []*MuxRoute
}

func ParamEqual(idx int, param string) func(*ircv3.Message) bool {
	return func(m *ircv3.Message) bool {
		return m.Param(idx) == param
	}
}

func (m *Mux) Handle(matchers ...func(*ircv3.Message) bool) func(MessageHandler) {
	return func(h MessageHandler) {
		m.routes = append(m.routes, &MuxRoute{
			matchers: matchers,
			handler:  h,
		})
	}
}
func (m *Mux) HandleFunc(matchers ...func(*ircv3.Message) bool) func(MessageHandlerFunc) {
	return func(h MessageHandlerFunc) {
		m.Handle(matchers...)(h)
	}
}

func (m *Mux) Cmd(cmd string, extra ...func(*ircv3.Message) bool) func(MessageHandler) {
	return func(h MessageHandler) {
		m.routes = append(m.routes, &MuxRoute{
			matchers: append([]func(*ircv3.Message) bool{
				func(m *ircv3.Message) bool {
					return m.Command == cmd
				},
			}, extra...),
			handler: h,
		})
	}
}
func (m *Mux) CmdFunc(cmd string, extra ...func(*ircv3.Message) bool) func(MessageHandlerFunc) {
	return func(h MessageHandlerFunc) {
		m.Cmd(cmd, extra...)(h)
	}
}

func (m *Mux) OnMessage(ctx context.Context, codec ircv3.SendCodec, msg *ircv3.Message) error {
	for _, v := range m.routes {
		if v.Matches(msg) {
			err := v.handler.OnMessage(ctx, codec, msg)
			if err != nil {
				// TODO: implement recoverable vs non recoverable errors
				return err
			}
		}
	}
	return nil
}

type MuxRoute struct {
	matchers []func(*ircv3.Message) bool

	handler MessageHandler
}

func (m *MuxRoute) Matches(msg *ircv3.Message) bool {
	for _, v := range m.matchers {
		if !v(msg) {
			return false
		}
	}
	return true
}
