package main

import (
	"context"
	"log/slog"
	"net"
	"os"

	"anime.bike/slogutil/slogenv"
	"anime.bike/stint"

	"anime.bike/irc/pkg/core"
	"anime.bike/irc/pkg/ircev"
	"anime.bike/irc/pkg/ircv3"
	"go.uber.org/fx"
	"go.uber.org/fx/fxevent"
)

func start(ctx context.Context, log *slog.Logger) error {

	bc := core.BackendConfig{
		// all modern servers support 302, so let's send it
		DisableCap302: false,
		User: core.User{
			Nick:     "anime_bike_irc",
			Username: "anime_bike_irc",
			Realname: "bike",
			Hostname: "anime.bike",
			Server:   "anime",
		},
	}
	b := core.NewBackend(bc, log)
	conn, err := net.Dial("tcp", "irc.libera.chat:6667")
	if err != nil {
		return err
	}
	codec := ircv3.NewConnCodec(conn)
	c := ircev.NewClient(b)
	err = c.Start(ctx, codec)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	fx.New(
		fx.WithLogger(func(l *slog.Logger) fxevent.Logger {
			r := &fxevent.SlogLogger{Logger: l}
			r.UseLogLevel(slog.LevelDebug)
			r.UseErrorLevel(slog.LevelError)
			return r
		}),
		fx.Provide(
			func() *slog.Logger {
				return slog.New(stint.NewHandler(os.Stderr, &stint.Options{
					AddSource: true,
					Level:     slogenv.EnvLevel(),
				}))
			},
			func(lc fx.Lifecycle) context.Context {
				c, cn := context.WithCancel(context.Background())
				lc.Append(fx.Hook{
					OnStop: func(ctx context.Context) error {
						cn()
						return nil
					},
				})
				return c
			}),
		fx.Invoke(start),
	).Run()
}
