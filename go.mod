module anime.bike/irc

go 1.22.0

require (
	anime.bike/slogutil v0.0.0-20240603065252-938fd61854c6
	anime.bike/stint v0.0.0-20240603063236-cb0ffb200a52
	github.com/emirpasic/gods/v2 v2.0.0-alpha
	github.com/stretchr/testify v1.9.0
	github.com/valyala/bytebufferpool v1.0.0
	go.uber.org/fx v1.22.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/dig v1.17.1 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	go.uber.org/zap v1.26.0 // indirect
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
